<?php
/*
 * @file video_i_ua.inc
 * Provides video.i.ua integration for emvideo module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_VIDEO_I_UA_MAIN_URL', 'http://video.i.ua/');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_video_i_ua_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_VIDEO_I_UA_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_video_i_ua_info() {
  $features = array(
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'video_i_ua',
    'name' => t('video.i.ua'),
    'url' => EMVIDEO_VIDEO_I_UA_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !video_i_ua.', array('!video_i_ua' => l(t('video.i.ua'), EMVIDEO_VIDEO_I_UA_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['video_i_ua'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_video_i_ua_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the video_i_ua provider.
  $form['video_i_ua']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the video_i_ua provider.
  $form['video_i_ua']['player_options']['emvideo_video_i_ua_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_video_i_ua_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_video_i_ua_extract($parse = '') {
  // Here we assume that a URL will be passed in the form of
  // http://video.i.ua/video/text-video-title
  // or embed code in the form of <object value="http://video.i.ua/embed...".

  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  /*
  urls are
   http://video.i.ua/user/1616937/16951/87569/
   */
  return array(
    // In this expression, we're looking first for text matching the expression
    // between the @ signs. The 'i' at the end means we don't care about the
    // case. Thus, if someone enters http://video.i.ua, it will still
    // match. We escape periods as \., as otherwise they match any character.
    // The text in parentheses () will be returned as the provider video code,
    // if there's a match for the entire expression. In this particular case,
    // ([^?]+) means to match one more more characters (+) that are not a
    // question mark ([^\?]), which would denote a query in the URL.
    '@video\.i\.ua/user/([0-9/]+)/@i',

  );
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the video_i_ua provider.
 */
function emvideo_video_i_ua_data($field, $item) {
  // Initialize the data array.
  $data = array();

  // Create some version control. Thus if we make changes to the data array
  // down the road, we can respect older content. If allowed by Embedded Media
  // Field, any older content will automatically update this array as needed.
  // In any case, you should account for the version if you increment it.
  $data['emvideo_video_i_ua_version'] = EMVIDEO_VIDEO_I_UA_DATA_VERSION;

  $html = _media_video_i_ua_fetch_page('http://video.i.ua/user/'. $item['value']);
  if ($html) {
    if (preg_match_all('@link href="http\://([^"]+)" rel="image_src"@', $html, $matches) ||
        preg_match_all('@link rel="image_src" href="http\://([^"]+)"@', $html, $matches)) {
      $data['thumbnail'] = 'http://'. $matches[1][0];
    }
    $html_match = '@showFlash\("[^"]+", "([^"]+)",@';
    if (preg_match_all($html_match, $html, $matches, PREG_PATTERN_ORDER)) {
      $data['flash_vars'] = $matches[1][0];
    }
  }
  return $data;
}

/**
 *  hook emvideo_PROVIDER_rss
 *
 *  This attaches a file to an RSS feed.
 */
function emvideo_video_i_ua_rss($item, $teaser = NULL) {
  if ($item['value']) {
    $file['thumbnail']['filepath'] = $item['data']['thumbnail'];

    return $file;
  }
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_video_i_ua_embedded_link($video_code) {
  return 'http://video.i.ua/user/'. $video_code;
}

/**
 * The embedded flash displaying the video_i_ua video.
 */
function theme_emvideo_video_i_ua_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    list($user_id, $junk, $video_id) = explode('/', $item['value']);
    $flash_vars = $item['data']['flash_vars'];
    if (!$flash_vars) {
      return t('There was an error displaying the video');
    }
    /*
     <object width="640" height="372" 
     codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"
     classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
     <param value="true" name="allowFullScreen"><param value="transparent" name="wmode">
     <param value="userID=1616937&amp;videoID=87569&amp;playTime=25&amp;videoSize=1606316&amp;key=470526033&amp;keyTime=1289875092" name="FlashVars">
     <param value="http://i3.i.ua/video/vpb.swf?14" name="movie"><param value="always" name="AllowScriptAccess">
     <param value="high" name="quality">
     <param value="#000000" name="bgcolor">
     <embed width="640" height="372" align="middle" pluginspage="http://www.macromedia.com/go/getflashplayer"
     type="application/x-shockwave-flash"
     name="test_draw" allowscriptaccess="always"
     allowfullscreen="true"
     bgcolor="#000000" wmode="transparent"
     quality="high"
     flashvars="userID=1616937&amp;videoID=87569&amp;playTime=25&amp;videoSize=1606316&amp;key=470526033&amp;keyTime=1289875092" src="http://i3.i.ua/video/vpb.swf?14"></object>
    */
    /*
     <OBJECT width="450" height="349">
      <PARAM name="movie" value="http://i.i.ua/video/evp.swf?V=15611.18ac29.19.1882ac.k900aeac7"></PARAM>
      <EMBED src="http://i.i.ua/video/evp.swf?V=15611.18ac29.19.1882ac.k900aeac7"
      type="application/x-shockwave-flash" width="450" height="349"></EMBED></OBJECT>*/
    $autoplay = $autoplay ? 'true' : 'false';
    $fullscreen = variable_get('emvideo_myvi_full_screen', 1) ? 'true' : 'false';
    $output = '<object width="'. $width .'" height="'. $height .'" 
     codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0"
     classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
     <param value="'. $fullscreen .'" name="allowFullScreen"><param value="transparent" name="wmode">
     <param value="'. $flash_vars .'" name="FlashVars">
     <param value="http://i3.i.ua/video/vpb.swf?14" name="movie"><param value="always" name="AllowScriptAccess">
     <param value="high" name="quality">
     <param value="#000000" name="bgcolor">
     <embed width="'. $width .'" height="'. $height .'" pluginspage="http://www.macromedia.com/go/getflashplayer"
     type="application/x-shockwave-flash"
     allowscriptaccess="always"
     allowfullscreen="'. $fullscreen .'"
     bgcolor="#000000" wmode="transparent"
     quality="high"
     flashvars="'. $flash_vars .'" src="http://i3.i.ua/video/vpb.swf?14"></object>';
  }
  return $output;
}

/**
 * hook emvideo_PROVIDER_thumbnail
 * Returns the external url for a thumbnail of a specific video.
 *  @param $field
 *    The field of the requesting node.
 *  @param $item
 *    The actual content of the field from the requesting node.
 *  @return
 *    A URL pointing to the thumbnail.
 */
function emvideo_video_i_ua_thumbnail($field, $item, $formatter, $node, $width, $height) {
  // In this demonstration, we previously retrieved a thumbnail using oEmbed
  // during the data hook.
  return $item['data']['thumbnail'];
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_video_i_ua_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_video_i_ua_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_video_i_ua_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('emvideo_video_i_ua_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  Implementation of hook_emfield_subtheme.
 *  This returns any theme functions defined by this provider.
 */
function emvideo_video_i_ua_emfield_subtheme() {
  $themes = array(
      'emvideo_video_i_ua_flash'  => array(
          'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
          'file' => 'providers/video_i_ua.inc',
          // If you don't provide a 'path' value, then it will default to
          // the emvideo.module path. Obviously, replace 'emvideo_i_ua' with
          // the actual name of your custom module.
          'path' => drupal_get_path('module', 'media_video_i_ua'),
      )
  );
  return $themes;
}

/**
 * Util function to get html from myvi.ru page
*/
function _media_video_i_ua_fetch_page($url) {
  $cache_id = 'media_video_i_ua_'. $url;
  $cache = cache_get($cache_id);
  if ($cache && $cache->data) {
    $html = $cache->data;
  }
  else {
    $result = drupal_http_request($url);
    $code   = floor($result->code / 100) * 100;
    $types  = array('text/html');
    if ($result->data && $code != 400 && $code != 500 && in_array($result->Content-Type, $types)) {
      cache_set($cache_id, $result->data);
      $html = $result->data;
    }
    else  {
      //if we are unsuccessful then log a message in watchdog
      watchdog('media_video_i_ua', 'The html '. $url .' could not be retrieved');
      return FALSE;
    }    
  }
  return $html;
}